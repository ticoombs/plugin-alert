#!/usr/bin/env python3

'''

'''
from imaplib import IMAP4_SSL
from mail_extract import get_plugin_name
import util_db
import email
import queue
import threading
import time


class myMailThread(threading.Thread):
    def __init__(self, threadID, name, q, user, password):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.user = user
        self.password = password
        self.q = q
        # Login on Initialisation
        tMail = IMAP4_SSL("slowb.ro")
        self.mail = tMail
        self.login()

    def run(self):
        # print("Starting " + self.name)
        process_queue(self.name, self.mail, self.q)
        self.logout()
        # print("Exiting " + self.name)

    def login(self):
        self.mail.login(self.user, self.password)
        # Change to the correct Folder
        self.mail.select(vulnDBDirectory)

    def logout(self):
        self.mail.close()
        self.mail.logout()

    def get_messages(self):
        try:
            self.mail.select(vulnDBDirectory)
            typ, data = self.mail.search(None, 'UNSEEN FROM noreply@wpvulndb.com')
            print("Total Unseen: {}".format(len(data[0].split())))
            for num in data[0].split():
                queueLock.acquire()
                # print("Adding: {} to the queue".format(num))
                workQueue.put(num)
                queueLock.release()
        except Exception:
            print("OOps")


def process_queue(threadName, mail, q):
    while not exitFlag:
        queueLock.acquire()
        if workQueue.empty():
            queueLock.release()
            # print("%s Queue is Empty, waiting for more data" % (threadName))
            time.sleep(1)
            continue

        num = q.get()
        queueLock.release()
        # print("{} - Fetching {}".format(threadName, num))
        typ, data = mail.fetch(num, '(RFC822)')
        # print("{} - Decoding: {}".format(threadName, data))
        msg = email.message_from_string(data[0][1].decode('utf-8'))
        # print("{} - Fetching {}".format(threadName, msg))
        typ, data = mail.store(num, '+FLAGS', '\\Seen')

        subject = str(email.header.make_header(
            email.header.decode_header(msg['Subject']))).replace('\n', '').replace('\r', '')
        plugin = get_plugin_name(subject.strip())
        if not plugin:
            print("%s - Failed - %s " % (threadName, subject))
            continue
        conn = util_db.db_sqlite()
        conn.add_plugin(plugin, "plugin")
        # print("%s - Processed - %s" % (threadName, plugin))


if __name__ == '__main__':

    exitFlag = 0
    parserName = "Parser-"
    nameList = "Query"
    workQueue = queue.Queue(0)
    queueLock = threading.Lock()
    threads = []
    threadID = 1

    vulnDBDirectory = "MailingLists.WPVulnDB"

    user = "username"
    password = "password"
    totalVulns = {}

    # Build our singular query thread and update our workQueue
    thr = myMailThread(threadID, nameList, workQueue, user, password)
    thr.get_messages()
    threadID += 1

    # Start all our processing threads
    for x in range(0, 3):
        tName = "{}{}".format(parserName, x)
        thread = myMailThread(threadID, tName, workQueue, user, password)
        thread.start()
        threads.append(thread)
        threadID += 2

    # Wait until the queue is empty to quit
    while not workQueue.empty():
        pass

    exitFlag = 1
    for t in threads:
        t.join()
    print("Exiting Main Thread")
