#!/usr/bin/env python3

import re

def get_plugin_name(subject):
    # FooBox Image Lightbox <= 1.0.4 - XSS
    # N-Media Website Contact Form with File Upload <= 1.3.4 - Arbitrary\r\n File Upload
    # Insta Gallery < 2.4.8 - CSRF & Missing Authorisation Checks
    # Ajax Search Pro - Authenticated RCE
    # iThemes Security 3.0-4.6.12 – Stored Cross-Site Scripting (XSS)

    # "plugin (<|<=) [0-9\.\-]+ - (.*)"
    # pattern = re.compile("(?P=plugin)[A-Za-z\-\ ]+\
    #        \ (\<\=)?{0,2}\ \
    #        (?P=version)([0-9\.\-]+)?\ \-\ \
    #        (?P+vuln)[A-Za-z\ ]+")
    # print("Testing: {}".format(subject))
    # plugin_re = "(?P<plugin>[A-Za-z\-\ \,\.\&\|]+)"
    # version_re = "(?P<version>([0-9\.\-]+)?)?"
    # vuln_re = "(?P<vuln>[A-Za-z\-\ \—\(\)\&]+)"

    pattern = "(?P<plugin>.*[A-Za-z\-\ \,\.\&\|]+)(\ (\<\=?){0,1})?\ v?(?P<version>([0-9\.\-]+)?)([\-\–\ ]+)(?P<vuln>[A-Za-z\-\ \(\)\&]+.*)"

#    if "<" in subject or "=" in subject:
#        pattern = re.compile("%s(\ (\<\=?){0,1})?\ %s\ ([\-\—\ ])?%s" % (plugin_re, version_re, vuln_re))
#    elif re.match("%s" % (version_re), subject):
#        pattern = re.compile("%s%s\ ([\-\—\ ])?%s" % (plugin_re, version_re, vuln_re))

    plugin = re.match(pattern, subject)
    if plugin:
        return(plugin.groupdict())
    else:
        return(plugin)
